﻿
#include <iostream>

const double lx = 1;
const double ly = 1;

inline double a1(double u)
{
	return u * u * u;
}
inline double a2(double u)
{
	return u * u * u;
}
inline double TrueSol(double x, double y)
{
	return y * sin(x) * (lx - x) * (ly - y);
}

inline double f(double x, double y)
{
	return 2. * pow(sin(x), 4) * pow((lx - x), 4) * pow(y, 3) * pow((ly - y), 3) - 3. * (sin(x) * (lx - x) * (ly - y)
		- y * sin(x) * (lx - x)) * y * y * pow(sin(x), 3) * pow((lx - x), 3) * pow((ly - y), 3) + 3 * (sin(x) * (lx - x) * (ly - y)
			- y * sin(x) * (lx - x)) * y * y * y * pow(sin(x), 3) * pow((lx - x), 3) * pow((ly - y), 2) - (-y * sin(x) * (lx - x) * (ly - y)
				- 2. * y * cos(x) * (ly - y)) * pow(y, 3) * pow(sin(x), 3) * pow((lx - x), 3) * pow((ly - y), 3)
		- 3 * (y * cos(x) * (lx - x) * (ly - y) - y * sin(x) * (ly - y)) * pow(y, 3) * pow(sin(x), 2) * pow((lx - x), 3) * pow((ly - y), 3) * cos(x)
		+ 3. * (y * cos(x) * (lx - x) * (ly - y) - y * sin(x) * (ly - y)) * pow(y, 3) * pow(sin(x), 3) * pow((lx - x), 2) * pow((ly - y), 3);
}
//fi(0,y)
inline double phi1(double y)
{
	return 0;
}
//fi(1,y)
inline double phi2(double y)
{
	return 0;
}
//fi(x,0)
inline double phi3(double x)
{
	return 0;
}
//fi(x,1)
inline double phi4(double x)
{
	return 0;
}

//u(i,j)/du(i,j)
double du1(double* u, int i, int j, double hx, double hy, int M, int N)
{
	return (6. * u[j * N + i] * (u[j * N + i + 1] * u[j * N + i + 1] - 2. * u[j * N + i] * u[j * N + i + 1] + u[j * N + i] * u[j * N + i])
		+ 6. * u[j * N + i] * u[j * N + i] * (u[j * N + i] - u[j * N + i + 1]) +
		3. * u[j * N + i] * u[j * N + i] * (u[j * N + i + 1] - 2. * u[j * N + i] + u[j * N + i - 1]) - 2. * u[j * N + i] * u[j * N + i] * u[j * N + i]) / hx / hx
		+ (6. * u[j * N + i] * (u[(j + 1) * N + i] * u[(j + 1) * N + i] - 2 * u[(j + 1) * N + i] * u[j * N + i] + u[j * N + i] * u[j * N + i])
			+ 6. * u[j * N + i] * u[j * N + i] * (u[j * N + i] - u[(j + 1) * N + i])
			+ 3. * u[j * N + i] * u[j * N + i] * (u[(j + 1) * N + i] - 2. * u[j * N + i] + u[(j - 1) * N + i]) - 2. * u[j * N + i] * u[j * N + i] * u[j * N + i]) / hy / hy;
}
//u(i,j)/du(i+1,j)
double du2(double* u, int i, int j, double hx, double hy, int M, int N)
{
	return u[j * N + i] * u[j * N + i] * (6. * u[j * N + i + 1] - 5. * u[j * N + i]) / hx / hx;

}
//u(i,j)/du(i-1,j)
double du3(double* u, int i, int j, double hx, double hy, int M, int N)
{
	return u[j * N + i] * u[j * N + i] * u[j * N + i] / hx / hx;

}
//u(i,j)/du(i,j+1)
double du4(double* u, int i, int j, double hx, double hy, int M, int N)
{
	return u[j * N + i] * u[j * N + i] * (6. * u[(j + 1) * N + i] - 5. * u[j * N + i]) / hy / hy;

}
//u(i,j)/du(i,j-1)
double du5(double* u, int i, int j, double hx, double hy, int M, int N)
{
	return u[j * N + i] * u[j * N + i] * u[j * N + i] / hy / hy;

}
//u(i,j)
double u(double* u, int i, int j, double hx, double hy, int M, int N)
{
	return (3. * u[j * N + i] * u[j * N + i] * (u[j * N + i + 1] * u[j * N + i + 1] - 2. * u[j * N + i] * u[j * N + i + 1] + u[j * N + i] * u[j * N + i]) + u[j * N + i] * u[j * N + i] * u[j * N + i] * (u[j * N + i + 1] - 2. * u[j * N + i] + u[j * N + i - 1])) / hx / hx
		+ (3. * u[j * N + i] * u[j * N + i] * (u[(j + 1) * N + i] * u[(j + 1) * N + i] - 2. * u[(j + 1) * N + i] * u[j * N + i] + u[j * N + i] * u[j * N + i]) + u[j * N + i] * u[j * N + i] * u[j * N + i] * (u[(j + 1) * N + i] - 2. * u[j * N + i] + u[(j - 1) * N + i])) / hy / hy + f(i * hx, j * hy);

}
//N=M only,
double NewtonMethod(int N, int M, double hx, double hy)
{
	int n = (N - 1) * (N - 1);
	double* uPrev = new double[(N+1)*(M+1)];
	double* uCurr = new double[(N + 1) * (M + 1)];
	double** arrA = new double* [n];
	double* arrF = new double[n];
	for (int i = 0; i < n; i++)
	{
		arrA[i] = new double[n];
	}
	//начальная точка
	for (int j = 1; j < N; j++)
	{
		for (int i = 1; i < M; i++)
		{
			uPrev[j * (N + 1) + i] = 0.;
		}
	}
	for (int i = 0; i <= N; i++)
	{
		uPrev[i] = phi3(i * hx);
		uPrev[N*(N+1)+i] = phi4(i * hx);
		uPrev[i * (N+1)] = phi1(i * hy);
		uPrev[i * (N+1) + N] = phi3(i * hy);
		uCurr[i] = phi3(i * hx);
		uCurr[N*(N+1)+i] = phi4(i * hx);
		uCurr[i * (N+1)] = phi1(i * hy);
		uCurr[i * (N+1) + N] = phi3(i * hy);
	}
	/*for (int i = 0; i <= N; i++)
	{
		for (int j = 0; j <= N; j++)
		{
			std::cout << uPrev[j*(N+1) + i] << "\t\t\t";
		}
		std::cout << std::endl;
	}*/
	double pogr;
	do
	{
		pogr = 0.;
		//заполняем значения правой части уравнения(дописать граничные условия != 0)
		for (int j = 0; j < N - 1; j++)
		{
			for (int i = 0; i < M - 1; i++)
			{
				arrF[j * (N - 1) + i] = -u(uPrev, i + 1, j + 1, hx, hy, M - 1, N + 1);
			}
		}


		for (int j = 0; j < n; j++)
		{
			for (int i = 0; i < n; i++)
			{

				if ((i - j) == -(N - 1))
				{
					arrA[i][j] = du4(uPrev, i % (N - 1) + 1, i / (N - 1) + 1, hx, hy, M - 1, N + 1);

				}
				else if ((i - j) == (N - 1))
				{
					arrA[i][j] = du5(uPrev, i % (N - 1) + 1, i / (N - 1) + 1, hx, hy, M - 1, N + 1);

				}
				else if (j % (N - 1) != (N - 2) && i - j == 1)
				{
					arrA[i][j] = du3(uPrev, i % (N - 1) + 1, i / (N - 1) + 1, hx, hy, M - 1, N + 1);
				}
				else if (i - j == -1 && i % (N - 1) != (N - 2))
				{
					arrA[i][j] = du2(uPrev, i % (N - 1) + 1, i / (N - 1) + 1, hx, hy, M - 1, N + 1);

				}
				else if (j == i)
				{
					arrA[i][j] = du1(uPrev, i % (N - 1) + 1, i / (N - 1) + 1, hx, hy, M - 1, N + 1);
				}
				else
				{
					arrA[i][j] = 0;

				}

			}
		}

	/*	for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
			{
				std::cout << arrA[j][i] << "\t";
			}
			std::cout << std::endl;
		}
		std::cout << "\n\n" << du1(uPrev, 2, 2, hx, hy, M, N + 1);*/



		const double eps = 0.00000001;
		for (int k = 0; k < n; k++)
		{


			for (int i = k; i < n; i++)
			{
				double temp = arrA[i][k];
				if (abs(temp) < eps) continue;
				for (int j = 0; j < n; j++)
					arrA[i][j] /= temp;
				arrF[i] /= temp;
				if (i == k)  continue;
				for (int j = 0; j < n; j++)
					arrA[i][j] -= arrA[k][j];
				arrF[i] -= arrF[k];
			}

		}
		// обратная подстановка
		for (int k = n - 1; k >= 0; k--)
		{
			uCurr[k % (N - 1) + 1 + (k / (N - 1) + 1) * (N + 1)] = arrF[k];
			for (int i = 0; i < k; i++)
				arrF[i] -= arrA[i][k] * uCurr[k % (N - 1) + 1 + (k / (N - 1) + 1) * (N + 1)];
		}

		//for (int j = 1; j < N; j++)
		//{
		//	
		//	for (int i = 1; i < N; i++)
		//	{
		//		if (abs(uCurr[j * (N + 1) + i] - uPrev[j * (N + 1) + i]) > pogr)
		//		{
		//			pogr = abs(uCurr[j * (N + 1) + i] - uPrev[j * (N + 1) + i]);
		//		}
		//		//cout << arrU[j * (N - 1) + i] << '\t' << TrueSol((i + 1) * dx, (j + 1) * dy) << endl;
		//		uPrev[j * (N + 1) + i] = uCurr[j * (N + 1) + i];
		//	}
		//}
		for (int j = 1; j < N; j++)
		{
			for (int i = 1; i < N; i++)
			{
				double pogrTemp = abs(uCurr[j * (N + 1) + i] - TrueSol(i * hx, j * hy));
				if (pogrTemp > pogr)
				{
					pogr = pogrTemp;

				}
				//std::cout << uCurr[j * (N + 1) + i] << '\t' << TrueSol((i)*hx, (j)*hy) << std::endl;
				uPrev[j * (N + 1) + i] = uCurr[j * (N + 1) + i];
			}
		}
		std::cout << pogr << std::endl;
		//std::cin >> pogr;
	}
	while (pogr>0.001);
	pogr = 0.;
	for (int j = 1; j < N; j++)
	{
		for (int i = 1; i < N; i++)
		{
			double pogrTemp = abs(uCurr[j * (N + 1) + i] - TrueSol(i*hx,j*hy));
			if (pogrTemp > pogr)
			{
				pogr = pogrTemp;

			}
			std::cout << uCurr[j * (N + 1) + i] << '\t' << TrueSol((i) * hx, (j) * hy) << std::endl;
			
		}
	}
	std::cout << std::endl<< pogr<< std::endl;

	return 0;
}


//N=M only,
double SORNewtonMethod(int N, int M, double hx, double hy)
{
	int n = (N - 1) * (N - 1);
	double** uPrev = new double*[M + 1];
	double** uCurr = new double*[M + 1];
	double** arrF = new double*[M + 1];
	for (int i = 0; i < M + 1; i++)
	{
		uPrev[i] = new double[N + 1];
		uCurr[i] = new double[N+1];
		arrF[i] = new double[N+1];
		
	}
	//начальная точка
	for (int i = 1; i < M; i++)
	{
		for (int j = 1; j < N; j++)
		{
			uPrev[i][j] = 0.;
		}
	}
	for (int i = 0; i <= N; i++)
	{
		uPrev[0][i] = phi3(i * hx);
		uPrev[N][i] = phi4(i * hx);
	}
	for (int i = 0; i <= M; i++)
	{
		uPrev[i][0] = phi1(i * hy);
		uPrev[i][M] = phi2(i * hy);
	}
	/*for (int i = 0; i <= N; i++)
	{
		for (int j = 0; j <= N; j++)
		{
			std::cout << uPrev[j*(N+1) + i] << "\t\t\t";
		}
		std::cout << std::endl;
	}*/
	double pogr=0;
	double omega = 1.;
	do
	{
		double pogrTemp = 0;
		for (int i = 1; i < M; i++)
		{
			for (int j = 1; j < N; i++)
			{
				pogrTemp = uPrev[i][j];
				uPrev[j * (N + 1) + i] = (-omega * (uPrev[(j - 1) * (N + 1) + i] * du5(uPrev, i, j - 1, hx, hy, M + 1, N + 1) + uPrev[(j * (N + 1) + i - 1)] * du3(uPrev, i - 1, j, hx, hy, M + 1, N + 1)) + (1 - omega) * du1(uPrev, i, j, hx, hy, M + 1, N + 1) * uPrev[j * (N + 1) + i]
					- omega * (uPrev[j * (N + 1) + i + 1] * du2(uPrev, i + 1, j, hx, hy, M + 1, N + 1) + uPrev[(j + 1) * (N + 1) + i] * du4(uPrev, i, j + 1, hx, hy, M + 1, N + 1)) + omega * u(uPrev, i, j, hx, hy, M + 1, N + 1)) / du1(uPrev, i, j, hx, hy, M + 1, N + 1);
				pogrTemp = abs(uPrev[j * (N + 1) + i] - pogrTemp);
				if(pogrTemp>pogr)
				{
					pogr = pogrTemp;
				}
			}
		}















		//pogr = 0.;
		////заполняем значения правой части уравнения(дописать граничные условия != 0)
		//for (int j = 0; j < N - 1; j++)
		//{
		//	for (int i = 0; i < M - 1; i++)
		//	{
		//		arrF[j * (N - 1) + i] = -u(uPrev, i + 1, j + 1, hx, hy, M - 1, N + 1);
		//	}
		//}


		//for (int j = 0; j < n; j++)
		//{
		//	for (int i = 0; i < n; i++)
		//	{

		//		if ((i - j) == -(N - 1))
		//		{
		//			arrA[i][j] = du4(uPrev, i % (N - 1) + 1, i / (N - 1) + 1, hx, hy, M - 1, N + 1);

		//		}
		//		else if ((i - j) == (N - 1))
		//		{
		//			arrA[i][j] = du5(uPrev, i % (N - 1) + 1, i / (N - 1) + 1, hx, hy, M - 1, N + 1);

		//		}
		//		else if (j % (N - 1) != (N - 2) && i - j == 1)
		//		{
		//			arrA[i][j] = du3(uPrev, i % (N - 1) + 1, i / (N - 1) + 1, hx, hy, M - 1, N + 1);
		//		}
		//		else if (i - j == -1 && i % (N - 1) != (N - 2))
		//		{
		//			arrA[i][j] = du2(uPrev, i % (N - 1) + 1, i / (N - 1) + 1, hx, hy, M - 1, N + 1);

		//		}
		//		else if (j == i)
		//		{
		//			arrA[i][j] = du1(uPrev, i % (N - 1) + 1, i / (N - 1) + 1, hx, hy, M - 1, N + 1);
		//		}
		//		else
		//		{
		//			arrA[i][j] = 0;

		//		}

		//	}
		//}

		///*	for (int i = 0; i < n; i++)
		//	{
		//		for (int j = 0; j < n; j++)
		//		{
		//			std::cout << arrA[j][i] << "\t";
		//		}
		//		std::cout << std::endl;
		//	}
		//	std::cout << "\n\n" << du1(uPrev, 2, 2, hx, hy, M, N + 1);*/



		//const double eps = 0.00000001;
		//for (int k = 0; k < n; k++)
		//{


		//	for (int i = k; i < n; i++)
		//	{
		//		double temp = arrA[i][k];
		//		if (abs(temp) < eps) continue;
		//		for (int j = 0; j < n; j++)
		//			arrA[i][j] /= temp;
		//		arrF[i] /= temp;
		//		if (i == k)  continue;
		//		for (int j = 0; j < n; j++)
		//			arrA[i][j] -= arrA[k][j];
		//		arrF[i] -= arrF[k];
		//	}

		//}
		//// обратная подстановка
		//for (int k = n - 1; k >= 0; k--)
		//{
		//	uCurr[k % (N - 1) + 1 + (k / (N - 1) + 1) * (N + 1)] = arrF[k];
		//	for (int i = 0; i < k; i++)
		//		arrF[i] -= arrA[i][k] * uCurr[k % (N - 1) + 1 + (k / (N - 1) + 1) * (N + 1)];
		//}

		////for (int j = 1; j < N; j++)
		////{
		////	
		////	for (int i = 1; i < N; i++)
		////	{
		////		if (abs(uCurr[j * (N + 1) + i] - uPrev[j * (N + 1) + i]) > pogr)
		////		{
		////			pogr = abs(uCurr[j * (N + 1) + i] - uPrev[j * (N + 1) + i]);
		////		}
		////		//cout << arrU[j * (N - 1) + i] << '\t' << TrueSol((i + 1) * dx, (j + 1) * dy) << endl;
		////		uPrev[j * (N + 1) + i] = uCurr[j * (N + 1) + i];
		////	}
		////}
		//for (int j = 1; j < N; j++)
		//{
		//	for (int i = 1; i < N; i++)
		//	{
		//		double pogrTemp = abs(uCurr[j * (N + 1) + i] - TrueSol(i * hx, j * hy));
		//		if (pogrTemp > pogr)
		//		{
		//			pogr = pogrTemp;

		//		}
		//		//std::cout << uCurr[j * (N + 1) + i] << '\t' << TrueSol((i)*hx, (j)*hy) << std::endl;
		//		uPrev[j * (N + 1) + i] = uCurr[j * (N + 1) + i];
		//	}
		//}
		std::cout << pogr << std::endl;
		////std::cin >> pogr;
	} while (pogr > 0.001);
	pogr = 0.;
	for (int j = 1; j < N; j++)
	{
		for (int i = 1; i < N; i++)
		{
			double pogrTemp = abs(uCurr[j * (N + 1) + i] - TrueSol(i * hx, j * hy));
			if (pogrTemp > pogr)
			{
				pogr = pogrTemp;

			}
			std::cout << uCurr[j * (N + 1) + i] << '\t' << TrueSol((i)*hx, (j)*hy) << std::endl;

		}
	}
	std::cout << std::endl << pogr << std::endl;

	return 0;
}

int main()
{
	int n;
	std::cin >> n;
	SORNewtonMethod(n, n, lx / n, ly / n);
	std::cout << "Hello World!\n";
}

